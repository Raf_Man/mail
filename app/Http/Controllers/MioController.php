<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NewContact;
use Illuminate\Support\Facades\Mail;

class MioController extends Controller
{

	public function form()
	{
		return view('mail.form');
	}

	public function sendEmail(Request $req)
	{
		$data = $req->except("_token");
		Mail::to($data['email'])->send(new NewContact($data));

		return redirect('/')
				->with(['status'=>'success','message'=>'Grazie per averci contattato! Controlla la tua casella di posta :)']);
	} 
}
