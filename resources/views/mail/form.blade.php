@extends('base')

@section('container')
  <div class="container">

    <form action="/" method="POST">

      @csrf

      <div class="form-group">
        <label for="exampleFormControlInput1">Email address</label>
        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
      </div>

      <div class="form-group">
        <label for="exampleFormControlInput1">Name</label>
        <input type="text" name="nome" class="form-control" id="exampleFormControlInput1" placeholder="Giacomo">
      </div>
      
      
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Example textarea</label>
        <textarea name="textarea" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
      </div>

      <button type="submit" class="btn btn-primary mb-2">Invia</button>

    </form>

  </div>
@endsection